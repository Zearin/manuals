*****************
Live Path Effects
*****************

Even more effects are available in the :guilabel:`Path Effects` dialog, which you can access as :guilabel:`Path Effects` from the bottom of the :guilabel:`Path` menu entry.

When you don't have a path selected, the dialog will prompt you to choose one before you can proceed. Click on the plus sign to see a list of available path effects. Select one of them, then click on :guilabel:`Add`. Now you will be presented with a list of options for the selected effect. In most cases, the path will already look different from before now.

You can adjust a path with a Live Path Effect by the options in its dialog, and also by manipulating the path's handles on the canvas.

Here's a description of some of the Path Effects (LPEs). Don't hesitate to discover the functionality of the others on your own:

Power Stroke
  Adds handles that allow you to change the width of the path, so it can be different at different points of the path.

Sketch
  Transforms your path into multiple lines that look like a sketch drawn with a pencil.

Perspective / Envelope
  Seriously helpful for drawing in perspective, thanks to the addition of 4 handles for the corners.

Knot
  Creates gaps in the bottom parts of a path, when they cross with another part of the path.

Hatches
  Transforms your shape into hatches that can be heavily customized.

Interpolate subpaths
  The principle is the same as for the :guilabel:`Interpolate` extension, but the LPE approaches the topic differently.

Pattern along Path
  The principle is similar to the extension with the same name, but the pattern parts will all be parts of the same path.

Now we have taken a small tour around all the effects that add functionality to Inkscape and allow you to automate specific tasks. This book is only an introduction to Inkscape. Do not hesitate to further explore the software once you feel at ease with its basic functionality.
