*********************
Professional Software
*********************

Inkscape is powerful but doesn't require a recent, high-end computer to be usable. It has many drawing tools, filters, effects and patterns.

Many professionals use Inkscape for vector image editing. It fits easily into a design workflow with open source or proprietary tools.

What am I allowed to do with it?
================================

Inkscape is **free and open source software** (`Wikipedia
<https://en.wikipedia.org/wiki/Free_and_open-source_software>`_). It
can be `downloaded <https://inkscape.org/releases/>`_ and installed on
:doc:`Windows <installing-on-windows>`, :doc:`Mac
<installing-on-mac>`, and :doc:`Linux <installing-on-linux>`. Copying,
distribution, and modification of the software are freely permitted.
Inkscape can be used at work or at home, for professional or personal
work. What you create with it is entirely yours, and you are free to distribute, share it, sell it, however you’d like — or keep it to yourself! Except if you used someone else’s work as a basis for yours, in which case this person is also a co-author of the work.

How much does it cost?
======================

Inkscape is **gratis**, that is, cost-free: you can download and
install it as many times as you want without paying either a fixed fee
or a subscription, and give it away to friends, provided you agree
with the free software GNU General Public License (GPL). The only
investment you'll need to make is to **learn** to use it and gain
**new skills**.

You can also `donate <https://inkscape.org/support-us/>`_ to allow the project volunteers to dedicate more time to it.
