*********************
Creating Custom Fonts
*********************

Fonts can be saved in different file formats: OTF, TTF and SVG (and some more).

With Inkscape, you can create SVG fonts. The letters, numbers and special characters
a font consists of are called :term:`glyphs <Glyph>`. These fonts should later be
converted to another font file format, to be used in a text editor or graphics program.
You will not be able to use them directly in a webpage, for instance.
SVG fonts have been removed from the official SVG standard.
Inkscape supports creating them for historical reasons and will keep that feature in future versions of it.

To create a custom SVG font:

#. Open the typography template with :menuselection:`File --> New from Template --> Typography Canvas`.
#. Open the :guilabel:`Font Editor` dialog from :menuselection:`Text --> SVG Font Editor`.
#. In the column labelled :guilabel:`Font`, click on :guilabel:`New` to create a font. You can double-click on the generic name of the font to change it.
#. Open the :guilabel:`Layers` dialog from :menuselection:`Layer --> Layers`.

Repeat the following for each glyph that you want to add to your font:

#. In the :guilabel:`Layers` dialog, add a new layer by clicking on the '+' icon.
   Name it after your letter. Select the layer in the dialog.
#. Now, in the :guilabel:`Font Editor`, in the tab :guilabel:`Glyphs`, click on
   :guilabel:`Add Glyph`. Double-click on the :guilabel:`Glyph name` field to
   name your glyph, e.g. call it 'Capital A' or 'Space'. In the
   :guilabel:`Matching String` field, enter the letter that it corresponds to.
#. Draw the path for your glyph on the canvas.
#. When you're happy with your glyph, select it, and also select the
   corresponding row in the dialog, then click on :guilabel:`Get curves from
   selection`.

.. Tip::
  You can always test your font by typing a text into the field at the bottom of
  the :guilabel:`SVG Font Editor` dialog and looking at the preview above it.

.. Hint::
  You can use the little 'eye' icons in the :guilabel:`Layers` dialog to hide the layers with the glyphs that you have already finished. To protect the completed glyph layers from accidental changes, use the 'lock' icons.

When you are done, save the file as an Inkscape SVG (this is Inkscape's standard file format).

Although this functionality is meant for typographers, amateurs, too, can quickly
get a working result and test their work as they go.

When your font is finished, you can use a software like `FontForge
<https://fontforge.github.io/>`_ where you import your SVG font and can export
it into different formats to be able to use it with other software.

.. figure:: images/font_editor.png
   :alt: Typography canvas
   :class: screenshot

   The typography template has just the right size for a single letter. It comes with a set of useful guides. On the right side, the letter 'a' has already been added to the SVG font.
